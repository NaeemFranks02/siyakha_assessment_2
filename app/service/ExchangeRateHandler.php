<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\service;

use GuzzleHttp\Client;
use App\ExchangeRate;

/**
 * Description of ExchangeRateHandler
 *
 * @author 27814
 */


class ExchangeRateHandler {
    
    private $jsonratesKey = "4aa8b36454ef81876c161b40c517e1af";
    
    public function getRates() {
        return ExchangeRate::all();
    }
    
    public function refreshRates() {
        $reqClient = new Client();
        $response =  $reqClient->request(
                    'GET', 
                    'http://www.apilayer.net/api/live?currencies=ZAR,USD,GBP,EUR,KES'.
                    '&access_key='.$this->jsonratesKey
                );
        
        if ($response->getStatusCode() === 200) {
            $data = (array)json_decode($response->getBody())->quotes;

            ExchangeRate::truncate();
            
            foreach ($data as $key => $rate) {
                $currency = substr($key, 3);
                $exchangeRate = new ExchangeRate;
                $exchangeRate->currency = $currency;
                $exchangeRate->rate = $rate;
                $exchangeRate->defaultRate = $this->getStandardRate($currency);
                $exchangeRate->comparedTo = substr($key, 0,3);
                $exchangeRate->surcharge = $this->getSurchange($currency);
                $exchangeRate->save();
            }
            
            return $data;
        } else {
            throw new Exception("API Call Failed!");
        }
    }
    
    private function getStandardRate($currency) {
        switch ($currency) {
            case "ZAR": return 1;
            case "USD": return 0.0808279;
            case "GBP": return 0.0527032;
            case "EUR": return 0.0718710;
            case "KES": return 7.81498;
            default: return null;
        }
    }
    
    private function getSurchange($currency) {
        switch ($currency) {
            case "USD": return 0.75;
            case "GBP": return 0.05;
            case "EUR": return 0.05;
            case "KES": return 0.025;
            default: return 0;
        }
    }
    
    public function getExchangeRate($currency) {
        $zar = ExchangeRate::where('currency', 'ZAR')->first();
        $forex = ExchangeRate::where('currency', $currency)->first();        
        return $forex->rate / $zar->rate;
    }
    
    
}
