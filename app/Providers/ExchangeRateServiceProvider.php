<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\service\ExchangeRateHandler;

class ExchangeRateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\ExchangeRateHandler', function ($app) {
          return new ExchangeRateHandler();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
