<?php

namespace App\Http\Controllers;

use App\ExchangeRate;
use Illuminate\Http\Request;
use App\service\ExchangeRateHandler;

class ExchangeRateController extends Controller
{
    
    protected $exchangeRateHandler;


    public function __construct(ExchangeRateHandler $exchangeRateHandler)
    {
        $this->exchangeRateHandler = $exchangeRateHandler;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExchangeRate  $exchangeRate
     * @return \Illuminate\Http\Response
     */
    public function show(ExchangeRate $exchangeRate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExchangeRate  $exchangeRate
     * @return \Illuminate\Http\Response
     */
    public function edit(ExchangeRate $exchangeRate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExchangeRate  $exchangeRate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExchangeRate $exchangeRate)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExchangeRate  $exchangeRate
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExchangeRate $exchangeRate)
    {
        //
    }
    
    public function refreshRates() {
        $this->exchangeRateHandler->refreshRates();
        return redirect('/home');
    }
    
    public function calculateRates(Request $request) {
        $currency = ExchangeRate::find($request->currency);
        $zarRate = $request->purchase_fee;
        $foreignAmt = $request->purcase_qty; 
        $exchangeRate = $request->conversionType === 'Default' ?
                $currency->defaultRate : 
                $this->exchangeRateHandler->getExchangeRate($currency->currency);
        
        
        if ($request->changed_key === 'purchase_qty') {
            $zarRate = $request->purcase_qty /  $exchangeRate;
        } else {
            $foreignAmt = $request->purchase_fee * $exchangeRate;
        }
        
        return ["purchase_amount" => $foreignAmt, "purchase_fee" => $zarRate];
    }
}
