<?php

namespace App\Http\Controllers;

use App\PurchaseOrder;
use Illuminate\Http\Request;
use \App\service\ExchangeRateHandler;
use App\ExchangeRate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\Notifier;

class PurchaseOrderController extends Controller
{
    
    private $exchangeRateHandler;
    
    public function __construct(ExchangeRateHandler $exchangeRateHandler) {
        $this->exchangeRateHandler = $exchangeRateHandler;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('purchaseForm', ["exchangeRates" =>$this->exchangeRateHandler->getRates()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $purchaseOrder = new PurchaseOrder;
        $currency = ExchangeRate::find($request->currency);
        
        $purchaseOrder->orderedBy = Auth::user()->name;
        $purchaseOrder->currencyOrdered = $currency->currency;
        $purchaseOrder->exchangeRate = $request->conversionType === 'Default' ? $currency->defaultRate : $currency->rate;
        $purchaseOrder->orderFee = $request->purchase_fee;
        $purchaseOrder->amountOrdered = $request->purcase_qty;
        $purchaseOrder->surchargePercentage = $currency->surcharge * 100 . '%';
        $purchaseOrder->surchargeAmount = $purchaseOrder->purchase_fee * $currency->surcharge;
        
        $purchaseOrder->save();
        
        switch($currency->currency) {
            case "GBP":  Mail::to(Auth::user()->email)->send(new Notifier($purchaseOrder));               
                        break;
            case "EUR": 
                $po = new PurchaseOrder;
                $po->orderedBy = Auth::user()->name;
                $po->currencyOrdered = $currency->currency;
                $po->exchangeRate = $purchaseOrder->exchangeRate * 0.02;
                $po->orderFee = $request->purchase_fee;
                $po->amountOrdered = $request->purcase_qty;
                $po->surchargePercentage = $currency->surcharge * 100 . '%';
                $po->surchargeAmount = $po->purchase_fee * $currency->surcharge;

                $po->save();
                break;
        }
        
        return redirect('home')->with("created_order", $purchaseOrder);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseOrder $purchaseOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseOrder $purchaseOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PurchaseOrder $purchaseOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseOrder $purchaseOrder)
    {
        //
    }
}
