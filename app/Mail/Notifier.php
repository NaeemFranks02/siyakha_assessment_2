<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\PurchaseOrder;

class Notifier extends Mailable
{
    use Queueable, SerializesModels;
    
    private $purchaseOrder;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PurchaseOrder $purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from("naeemfranks01@gmail.com")
                    ->view('Mail.ConfirmationMail')
                    ->with(["purchaseOrder" => $this->purchaseOrder]);
    }
}
