<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    New Order Saved!
                </div>

                <div class="text-info">
                    <p>Hi, {{ Auth::user()->name }} </p>
                    <p>
                        Your order for {{ $purchaseOrder->amountOrdered }} {{ $purchaseOrder->currencyOrdered }} has been processed successfully!
                        
                    <div class="container">
                        <h5>Order Summary</h5>
                        <div class="row">
                            <div class="col-xs-4">
                                Currency
                            </div>
                            <div class="col-xs-8">{{ $purchaseOrder->currencyOrdered }}</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                Exchange Rate
                            </div>
                            <div class="col-xs-8">{{ $purchaseOrder->exchangeRate }}</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                Surcharge percentage
                            </div>
                            <div class="col-xs-8">{{ $purchaseOrder->surchargePercentage }}</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                Amount of foreign currency purchased
                            </div>
                            <div class="col-xs-8">{{ $purchaseOrder->amountOrdered }}</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                Amount to be paid in ZAR
                            </div>
                            <div class="col-xs-8">{{ $purchaseOrder->orderFee }}</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                Amount of surcharge.
                            </div>
                            <div class="col-xs-8">{{ $purchaseOrder->surchargeAmount }}</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                Order Date
                            </div>
                            <div class="col-xs-8">{{ $purchaseOrder->created_at }}</div>
                        </div>

                    </div>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
