@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if (session('created_order'))
                <div class="alert alert-success" role="alert">
                    {{ 'New Order created for ' . session('created_order')->amountOrdered . ' ' . session('created_order')->currencyOrdered  }}
                </div>
            @endif
            
            <div class="float-left tab">
                <a href="/purchaseorders/create" class="btn btn-info">Place Order</a> 
            </div>
            <table>
                <thead>
                    <tr>
                        <th>Order Date</th>
                        <th>Currency</th>
                        <th>Exchange Rate</th>
                        <th>Amount Purchased</th>
                        <th>Order Fee</th>
                        <th>Surcharge (%)</th>
                        <th>Surcharge Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                        <tr>
                            <td>{{ $order->created_at }}</td>
                            <td>{{ $order->currencyOrdered }}</td>
                            <td>{{ $order->ExchangeRate }}</td>
                            <td>{{ $order->amountOrdered }}</td>
                            <td>{{ $order->orderFee }}</td>
                            <td>{{ $order->surchargeAmount }}</td>
                            <td>{{ $order->surchargePercentage }}</td>
                            
                        </tr>
                    @endforeach
                </tbody>
            </table> 
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        
        $(document).ready(function() {
            $('.alert').alert();
            $('table').DataTable();
        });
    </script>
@endsection
