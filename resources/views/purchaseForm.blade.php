@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="float-left"><h3>Create New Order</h3></div>
                    <div class="float-right"><a href="/refresh-rates" class="btn btn-info">Refresh Exchange Rates</a></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="/purchaseorders">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="conversionType">Select Conversion Type</label>
                            <select class="form-control" name="conversionType" id="conversionType">
                                <option value="Default">Default Rates</option>
                                <option value="Real_Time">Real Time Rates</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="currency">Select Currency To Purchase</label>
                            <select class="form-control" name="currency" id="currency">
                                @foreach ($exchangeRates as $exchangeRate)
                                    @if ($exchangeRate->currency !== 'ZAR')
                                        <option value="{{ $exchangeRate->id }}">{{ $exchangeRate->currency }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="purchase_qty">Purchase Amount: </label>
                            <input type="number" step="any" class="form-control purchase-data" name="purcase_qty" id="purchase_qty" />
                        </div>
                        <div class="form-group">
                            <label for="purchase_fee">Fee (ZAR): </label>
                            <input type="number" step="any" class="form-control purchase-data" name="purchase_fee" id="purchase_fee" />
                        </div>
                        <div class="text-center">
                            <input type="submit" class="btn btn-success" value="Order">
                            <a href="/home" class="btn btn-danger">Cancel</a>
                        </div>
                        <input type="hidden" name="changed_key" id="changed_key" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    
    $(window).ready(() => {
        $('.purchase-data,.form-control').on('keyup', (e) => {
            $('#changed_key').val(e.target.id);
            $.post(
                '/calculate-purchase-values/', 
                $('form').serializeArray()
            ).then(data => {
                $('#purchase_fee').val(data.purchase_fee);
                $('#purchase_qty').val(data.purchase_amount);
            });
        });
    });
           
</script>
@endsection
  