<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExchangeRateMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Exchange_Rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('currency');
            $table->float('rate', 12, 8);
            $table->float('defaultRate', 12, 8);
            $table->float('surcharge');
            $table->string('comparedTo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Exchange_Rates');
    }
}
