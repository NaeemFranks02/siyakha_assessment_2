<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PurchaseOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Purchase_Orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('orderedBY');
            $table->string('currencyOrdered');
            $table->float('ExchangeRate', 12, 8);
            $table->float('orderFee', 12, 8);
            $table->float('amountOrdered', 12, 8);
            $table->string('surchargePercentage');
            $table->float('surchargeAmount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Purchase_Orders');
    }
}
