<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view((Auth::user() !== null) ? 'home' : 'auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/refresh-rates', 'ExchangeRateController@refreshRates');
Route::post('/calculate-purchase-values', 'ExchangeRateController@calculateRates');
Route::resource('purchaseorders', 'PurchaseOrderController');
//Route::post('/purchaseorders', 'PurchaseOrderController@store');